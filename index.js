var express = require('express');
var app = express();
var bodyparser = require("body-parser");
var handlebars = require('express-handlebars').create({ defaultLayout: 'main' });
var methodOverride = require('method-override');

app.engine('handlebars', handlebars.engine);
app.set('view engine', 'handlebars');

//***** Method Over riding to support put and delete */
app.use(methodOverride('_method', {methods:['POST','GET']} ));

//** using bodyparser in order to handling post requests */
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({
    extended: true
}));

//***** Restful Routing for products ****/
var productsRouter = express.Router();

productsRouter.get('/', function(req, res, next){
    console.log("get product");
    next();
});
productsRouter.route('/')
        .get(function(req,res,next){
            res.type('html');
            res.status(200);
            var queries = "";
            for(var key in req.query){
                queries += key + ": " + req.query[key] + '\n';
            }
            res.render("Products/index",{query: queries});
        })
        .post(function(req, res){
            for(var key in req.body){
                console.log(key + ": " + req.body[key] + '\n');
            }
            res.redirect('/products/new');
        });
productsRouter.route('/new')
        .get(function(req, res){
            res.type('html');
            res.status(200);
            res.render('Products/new');
        });
productsRouter.route('/:id/edit')
        .get(function(req,res){
            res.type('html');
            res.status(200);
            var currentProduct = {id:req.params.id, name:"Name", color:"#223344", price:2000};
            res.render("Products/edit", currentProduct);
        })
productsRouter.route('/:id')
        .get(function(req, res){
            res.type('html');
            res.status(200);
            res.render("Products/product", { id: req.params.id });
        })
        .put(function(req, res){
            console.log(`updating product with id: ${req.params.id}\nwith Values:\nname: ${req.body.name}\nprice: ${req.body.price}\ncolor: ${req.body.color}`);
            res.redirect(req.params.id);
        })
        .delete(function(req, res){
            console.log(`deleting product with id: ${req.params.id}`);
            res.redirect('/products');
        });
app.use('/products', productsRouter);

<<<<<<< HEAD



var userRouter = express.Router();

userRouter.get('/', function(req, res, next){
    console.log("get user");
    next();
});
userRouter.route('/')
        .get(function(req,res,next){
            res.type('html');
            res.status(200);
            var queries = "";
            for(var key in req.query){
                queries += key + ": " + req.query[key] + '\n';
            }
            res.render("users/index", {query: queries});
        })
        .post(function(req, res){
            for(var key in req.body){
                console.log(key + ": " + req.body[key] + '\n');
            }
            res.redirect('/users/new');
        });
userRouter.route('/new')
        .get(function(req, res){
            res.type('html');
            res.status(200);
            res.render('users/new');
        });
userRouter.route('/:id/edit')
        .get(function(req,res){
            res.type('html');
            res.status(200);
            var currentUser = {id:req.params.id, user: {name:"Kavian", lastName:"Rabbani"}};
            res.render("users/edit", currentUser);
        })
userRouter.route('/:id')
        .get(function(req, res){
            res.type('html');
            res.status(200);
            res.render("users/info", { id: req.params.id, user: {name:"Kavian", lastName:"Rabbani"}});
        })
        .put(function(req, res){
            if(req.body.newPass === req.body.oldPass){
                res.render(error, { err: "New password cant be same as the old one!", errCode: 12 })
            }
            console.log(`updating user with id: ${req.params.id}\nwith Values:\nName: ${req.body.name}\nLast name: ${req.body.lastName}\nOld password: ${req.body.oldPass}\nNew pass: ${req.body.newPass}`);
            res.redirect(req.params.id);
        })
        .delete(function(req, res){
            console.log(`deleting user with id: ${req.params.id}`);
            res.redirect('/users');
        });
app.use('/users', userRouter);


//******* Old Routing(Without Using Express Route Class) *******//
// app.get('/products/new', function(req, res){
//     res.type('html');
//     res.status(200);
//     res.render('Products/new')
// });
// app.get('/products/:id/edit', function(req, res){
//     res.type('html');
//     res.status(200);
//     var currentProduct = {id:req.params.id, name:"Name", color:"#223344",price:2000};
//     res.render("Products/edit", currentProduct);
// });
// app.get('/products/:id', function(req, res){
//     res.type('html');
//     res.status(200);
//     res.render("Products/product", { id: req.params.id });
// });
// app.put('/products/:id', function(req, res){
//     console.log(`updating product with id: ${req.params.id}\nwith Values:\nname: ${req.body.name}\nprice: ${req.body.price}\ncolor: ${req.body.color}`);
//     res.redirect(req.params.id);
// });
// app.delete('/products/:id', function(req, res){
//     console.log(`deleting product with id: ${req.params.id}`);
//     res.redirect('/products');
// });
// app.get('/products/', function(req,res,next){
//     res.type('html');
//     res.status(200);
//     var queries = "";
//     for(var key in req.query){
//         queries += key + ": " + req.query[key] + '\n';
//     }
//     res.render("Products/index",{query: queries});
// });
// app.post('/products', function(req, res){
//     for(var key in req.body){
//         console.log(key + ": " + req.body[key] + '\n');
//     }
//     res.redirect('/products/new');
// });

app.use('/', function(err, req, res, next){
    console.log("Error");
    res.type('html');
    res.status(500);
    res.render('error', {error: err});
});
app.get('/', function(req, res, next){
    var log = `ip: ${req.ip} \n`;
    for(var key in req.query){
        log += key + ": " + req.query[key] + '\n';
    }
    log += ` \n------------------------------------- \n  `;
    console.log(log);
    res.type('html');
    res.status(200);
    var babes = ["sweety", "beautiful", "KT", "nice man", "gorgeouos"];
    var rand = babes[Math.floor(babes.length * Math.random())]
    res.render('homepage', {babe: rand});
});

// Handling 404 pages
app.use('/', function(req, res, next){
    res.type('html');
    res.status(404);
    res.render('404', { route: req.url });
});
app.listen(3000);
